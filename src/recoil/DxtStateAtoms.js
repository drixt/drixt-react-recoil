import {atom, selector} from 'recoil';

/**
 * Base and Generic Recoil State Atoms / Selectors definition to reuse in the entire App.
 * This is a brief explanation of the usage:
 *
 * Atoms: units of the state
 * Selectors: derived and read only transformations of some states
 *
 * This file can have some generic definitions of loading states to use with other App components
 *
 * End
 */

export const globalLoadingArrayAtom = atom({key: 'globalLoadingArrayAtom', default: []});
export const localLoadingArrayAtom = atom({key: 'localLoadingArrayAtom', default: []});

export const isLocalLoadingSelector = selector({
  key: 'isLocalLoadingSelector',
  get: ({get}) => {
    const array = get(localLoadingArrayAtom);
    return array.length > 0;
  },
});

export const isGlobalLoadingSelector = selector({
  key: 'isGlobalLoadingSelector',
  get: ({get}) => {
    const array = get(globalLoadingArrayAtom);
    return array.length > 0;
  },
});
