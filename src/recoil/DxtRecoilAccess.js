import {useRecoilCallback} from 'recoil';

const nexus = {};

/**
 * Recoil's React Utility component to access Recoil Atoms / Selectors outside the React Components Life Cicle / Hooks.
 */
export default function DxtRecoilAccess() {
  nexus.get = useRecoilCallback(({snapshot}) =>
    function (atom) {
      return snapshot.getLoadable(atom).contents;
    }, []);

  nexus.getPromise = useRecoilCallback(({snapshot}) =>
    function (atom) {
      return snapshot.getPromise(atom);
    }, []);

  nexus.set = useRecoilCallback(({transact_UNSTABLE}) => {
    return function (atom, valOrUpdater) {
      transact_UNSTABLE(({set}) => {
        set(atom, valOrUpdater);
      });
    };
  }, []);

  nexus.reset = useRecoilCallback(({reset}) => reset, []);

  return null;
}

/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
export function getRecoilValue(atom) {
  return nexus.get(atom);
}

/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
export function getRecoilValuePromise(atom) {
  return nexus.getPromise(atom);
}

/**
 * Case using inside a React component, preffer: useSetRecoilState or useRecoilState.
 * See the docs!
 * @param atom
 * @param valOrUpdater
 */
export function setRecoilValue(atom, valOrUpdater) {
  nexus.set(atom, valOrUpdater)
}

/**
 * Case using inside a React component, preffer: useResetRecoilState or useRecoilState, to set a null value os something else.
 * See the docs!
 * @param atom
 */
export function resetRecoilValue(atom) {
  nexus.reset(atom)
}
