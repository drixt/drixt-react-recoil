import {globalLoadingArrayAtom, localLoadingArrayAtom} from "./DxtStateAtoms";
import {getRecoilValue, setRecoilValue} from "./DxtRecoilAccess";

function uuid() {
  let sep = '-';
  let uuid = '';

  for (let i = 0; i < 8; i++)
    uuid += Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + (i < 7 ? sep : "");

  return uuid;
}

/**
 * Try to release the promiseId if it is still in the array.
 * @param atom
 * @param promiseId
 */
function tryReleaseStuckPromiseId(atom: any, promiseId: string) {
  try {
    setTimeout(() => {
      const recoilValueArray = getRecoilValue(atom)
      if (recoilValueArray && !!recoilValueArray.find((rid: string) => rid === promiseId)) {
        const newRecoilValueArray = recoilValueArray.filter((rid: string) => rid !== promiseId)
        setRecoilValue(atom, newRecoilValueArray)
      }
    }, 3000)
  } catch (e) {
    console.error('>>>>>> tryReleaseStuckPromiseId ', e)
  }
}

export type DxtPromiseLoadingMode = 'local' | 'global'

/**
 * Simple shortcut to trigger the loading effect of GLOBAL or SKELETON until the promise is fullfiled.
 *
 * Returns the response data OR the catched error
 *
 * @param promise
 * @param loadingMode - 'local' or 'global'
 */
export async function fetchPromise<T>(promise: Promise<T>, loadingMode: DxtPromiseLoadingMode = 'local'): Promise<T> {
  const promiseId = uuid()
  const loadingStateAtom = loadingMode === 'local' ? localLoadingArrayAtom : globalLoadingArrayAtom

  setRecoilValue(loadingStateAtom, [...getRecoilValue(loadingStateAtom), promiseId])

  const res = await promise.catch((err) => ({hasError: true, ...err, ...(!!err.response ? err.response : {})}))

  setRecoilValue(
    loadingStateAtom,
    getRecoilValue(loadingStateAtom).filter((rid: string) => rid !== promiseId)
  )

  tryReleaseStuckPromiseId(loadingStateAtom, promiseId)

  return res
}
