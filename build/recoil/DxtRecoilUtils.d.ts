export type DxtPromiseLoadingMode = 'local' | 'global';
/**
 * Simple shortcut to trigger the loading effect of GLOBAL or SKELETON until the promise is fullfiled.
 *
 * Returns the response data OR the catched error
 *
 * @param promise
 * @param loadingMode - 'local' or 'global'
 */
export declare function fetchPromise<T>(promise: Promise<T>, loadingMode?: DxtPromiseLoadingMode): Promise<T>;
