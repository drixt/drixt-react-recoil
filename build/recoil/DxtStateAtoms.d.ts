/**
 * Base and Generic Recoil State Atoms / Selectors definition to reuse in the entire App.
 * This is a brief explanation of the usage:
 *
 * Atoms: units of the state
 * Selectors: derived and read only transformations of some states
 *
 * This file can have some generic definitions of loading states to use with other App components
 *
 * End
 */
export const globalLoadingArrayAtom: any;
export const localLoadingArrayAtom: any;
export const isLocalLoadingSelector: any;
export const isGlobalLoadingSelector: any;
