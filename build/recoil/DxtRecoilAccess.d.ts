/**
 * Recoil's React Utility component to access Recoil Atoms / Selectors outside the React Components Life Cicle / Hooks.
 */
export default function DxtRecoilAccess(): null;
/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
export function getRecoilValue(atom: any): any;
/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
export function getRecoilValuePromise(atom: any): any;
/**
 * Case using inside a React component, preffer: useSetRecoilState or useRecoilState.
 * See the docs!
 * @param atom
 * @param valOrUpdater
 */
export function setRecoilValue(atom: any, valOrUpdater: any): void;
/**
 * Case using inside a React component, preffer: useResetRecoilState or useRecoilState, to set a null value os something else.
 * See the docs!
 * @param atom
 */
export function resetRecoilValue(atom: any): void;
