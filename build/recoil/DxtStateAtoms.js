"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isGlobalLoadingSelector = exports.isLocalLoadingSelector = exports.localLoadingArrayAtom = exports.globalLoadingArrayAtom = void 0;
var recoil_1 = require("recoil");
/**
 * Base and Generic Recoil State Atoms / Selectors definition to reuse in the entire App.
 * This is a brief explanation of the usage:
 *
 * Atoms: units of the state
 * Selectors: derived and read only transformations of some states
 *
 * This file can have some generic definitions of loading states to use with other App components
 *
 * End
 */
exports.globalLoadingArrayAtom = (0, recoil_1.atom)({ key: 'globalLoadingArrayAtom', default: [] });
exports.localLoadingArrayAtom = (0, recoil_1.atom)({ key: 'localLoadingArrayAtom', default: [] });
exports.isLocalLoadingSelector = (0, recoil_1.selector)({
    key: 'isLocalLoadingSelector',
    get: function (_a) {
        var get = _a.get;
        var array = get(exports.localLoadingArrayAtom);
        return array.length > 0;
    },
});
exports.isGlobalLoadingSelector = (0, recoil_1.selector)({
    key: 'isGlobalLoadingSelector',
    get: function (_a) {
        var get = _a.get;
        var array = get(exports.globalLoadingArrayAtom);
        return array.length > 0;
    },
});
//# sourceMappingURL=DxtStateAtoms.js.map