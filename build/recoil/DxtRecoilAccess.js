"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resetRecoilValue = exports.setRecoilValue = exports.getRecoilValuePromise = exports.getRecoilValue = void 0;
var recoil_1 = require("recoil");
var nexus = {};
/**
 * Recoil's React Utility component to access Recoil Atoms / Selectors outside the React Components Life Cicle / Hooks.
 */
function DxtRecoilAccess() {
    nexus.get = (0, recoil_1.useRecoilCallback)(function (_a) {
        var snapshot = _a.snapshot;
        return function (atom) {
            return snapshot.getLoadable(atom).contents;
        };
    }, []);
    nexus.getPromise = (0, recoil_1.useRecoilCallback)(function (_a) {
        var snapshot = _a.snapshot;
        return function (atom) {
            return snapshot.getPromise(atom);
        };
    }, []);
    nexus.set = (0, recoil_1.useRecoilCallback)(function (_a) {
        var transact_UNSTABLE = _a.transact_UNSTABLE;
        return function (atom, valOrUpdater) {
            transact_UNSTABLE(function (_a) {
                var set = _a.set;
                set(atom, valOrUpdater);
            });
        };
    }, []);
    nexus.reset = (0, recoil_1.useRecoilCallback)(function (_a) {
        var reset = _a.reset;
        return reset;
    }, []);
    return null;
}
exports.default = DxtRecoilAccess;
/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
function getRecoilValue(atom) {
    return nexus.get(atom);
}
exports.getRecoilValue = getRecoilValue;
/**
 * Case using inside a React component, preffer: useRecoilValue or useRecoilState.
 * See the docs!
 * @param atom
 */
function getRecoilValuePromise(atom) {
    return nexus.getPromise(atom);
}
exports.getRecoilValuePromise = getRecoilValuePromise;
/**
 * Case using inside a React component, preffer: useSetRecoilState or useRecoilState.
 * See the docs!
 * @param atom
 * @param valOrUpdater
 */
function setRecoilValue(atom, valOrUpdater) {
    nexus.set(atom, valOrUpdater);
}
exports.setRecoilValue = setRecoilValue;
/**
 * Case using inside a React component, preffer: useResetRecoilState or useRecoilState, to set a null value os something else.
 * See the docs!
 * @param atom
 */
function resetRecoilValue(atom) {
    nexus.reset(atom);
}
exports.resetRecoilValue = resetRecoilValue;
//# sourceMappingURL=DxtRecoilAccess.js.map