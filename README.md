<h1>DRIXT - REACT RECOIL - v1.0.7</h1>

This is a base shortcut configurations of RecoilJs lib to use with React / ReactNative.

The setup prepares a basic config using Recoil and some utility configurations / functions to reuse on our projects.

Some useful scripts are:

- [DxtRecoilAccess.js](src/recoil/DxtRecoilAccess.js): React Wrapped Utility component to prograticaly access Recoil's states
  outside React components

- [DxtStateAtoms.js](src/recoil/DxtStateAtoms.js): Base Recoil Generic and Global Atoms and Selectors of general
  state, like state for global loading, local skeleton loading, etc.;

- [DxtRecoilUtils.js](src/recoil/DxtRecoilUtils.ts): Recoil's utility functions to use the base recoil atoms /
  selector with promises resolutions, eg, starts global or skeleton loading when promises are fetching, etc.
